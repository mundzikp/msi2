﻿namespace MSI2Project
{
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;

    public class TreePruner
    {
        public void Prune(Tree tree, IList<Instance> pruningSet)
        {
            new StatisticNode(tree.root, pruningSet).pruneSubTree();
        }

        private class StatisticNode
        {
            public StatisticNode(TreeNode connectedTreeNode, IList<Instance> instancesSet)
            {
                this.ConnectedTreeNode = connectedTreeNode;
                this.InstancesSet = instancesSet;
                this.Children = new List<StatisticNode>();
                this.ChildrenRatio = 0;
            }

            public void pruneSubTree()
            {        
                if (this.ConnectedTreeNode.Children == null)
                {
                    this.MostPopularCategory = this.ConnectedTreeNode.Category;
                    this.OwnRatio = this.InstancesSet.Count(instance => instance.category == this.MostPopularCategory);
                    return;
                }

                if(InstancesSet.Count == 0)
                {
                    return;
                }

                this.MostPopularCategory = DataLayer.GetMostPopularCategoryInSet(this.InstancesSet);
                this.OwnRatio = this.InstancesSet.Count(instance => instance.category == this.MostPopularCategory);

                switch (DataLayer.AttributesTypes[this.ConnectedTreeNode.SplitAttributeIndex])
                {
                    case AttributeType.Text:
                        {
                            foreach (var child in this.ConnectedTreeNode.Children)
                            {
                                var node = new StatisticNode(
                                        this.ConnectedTreeNode.Children[child.Key], this.InstancesSet.Where(
                                        instance => instance.attributes[this.ConnectedTreeNode.SplitAttributeIndex] == child.Key)
                                        .ToList());
                                this.Children.Add(node);
                                node.pruneSubTree();
                                ChildrenRatio += node.OwnRatio;

                            }
                            break;
                        }
                    case AttributeType.Numeric:
                        {
                            var child = this.ConnectedTreeNode.Children[string.Format("<{0}", this.ConnectedTreeNode.SplitValue)];
                            var node = new StatisticNode(
                                        child, this.InstancesSet.Where(
                                        instance => double.Parse(instance.attributes[this.ConnectedTreeNode.SplitAttributeIndex], CultureInfo.InvariantCulture) < child.SplitValue)
                                        .ToList());
                            this.Children.Add(node);
                            node.pruneSubTree();
                            ChildrenRatio += node.OwnRatio;

                            child = this.ConnectedTreeNode.Children[string.Format(">={0}", this.ConnectedTreeNode.SplitValue)];
                            node = new StatisticNode(
                                        child, this.InstancesSet.Where(
                                        instance => double.Parse(instance.attributes[this.ConnectedTreeNode.SplitAttributeIndex], CultureInfo.InvariantCulture) >= child.SplitValue)
                                        .ToList());
                            this.Children.Add(node);
                            node.pruneSubTree();
                            ChildrenRatio += node.OwnRatio;
                            break;
                        }
                }

                if(OwnRatio > ChildrenRatio)
                {
                    this.ConnectedTreeNode.Category = this.MostPopularCategory;
                    this.ConnectedTreeNode.Children = null;
                    this.ConnectedTreeNode.SplitAttributeIndex = 0;
                }
                else
                {
                    this.OwnRatio = this.ChildrenRatio;
                }
            }

            public TreeNode ConnectedTreeNode { get; set; }

            public double OwnRatio { get; set;}

            public double ChildrenRatio { get; set; }

            public string MostPopularCategory { get; set; }

            public bool IsLeaf { get; set; }

            public IList<StatisticNode> Children { get; set; }

            public IList<Instance> InstancesSet { get; set; }
            
        }
    }
}
