﻿namespace MSI2Project
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ChoseDataButton = new System.Windows.Forms.Button();
            this.ExternalSplitContainer = new System.Windows.Forms.SplitContainer();
            this.InternalControlSplitContainer = new System.Windows.Forms.SplitContainer();
            this.AttributesProcessedCountNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.CategorizedObservationsCountNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.CategorizeButton = new System.Windows.Forms.Button();
            this.ForestTreesCountNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.ForestTrainingObservationsCountNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.GenerateForestsButton = new System.Windows.Forms.Button();
            this.StatisticsLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.InternalGraphSplitContainer = new System.Windows.Forms.SplitContainer();
            this.TreeVisualizationSplitContainer = new System.Windows.Forms.SplitContainer();
            this.label2 = new System.Windows.Forms.Label();
            this.DisplayTreeButton = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.PrunningDisabledRadioButton = new System.Windows.Forms.RadioButton();
            this.PrunningEnalbedradioButton = new System.Windows.Forms.RadioButton();
            this.DrawnTreeIndexLabel = new System.Windows.Forms.Label();
            this.NextTreeButton = new System.Windows.Forms.Button();
            this.PreviousTreeButton = new System.Windows.Forms.Button();
            this.InformationGainGraphTypePanel = new System.Windows.Forms.Panel();
            this.GiniGraphRadioButton = new System.Windows.Forms.RadioButton();
            this.EntropyGraphRadioButton = new System.Windows.Forms.RadioButton();
            this.TreePicture = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.ExternalSplitContainer)).BeginInit();
            this.ExternalSplitContainer.Panel1.SuspendLayout();
            this.ExternalSplitContainer.Panel2.SuspendLayout();
            this.ExternalSplitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.InternalControlSplitContainer)).BeginInit();
            this.InternalControlSplitContainer.Panel1.SuspendLayout();
            this.InternalControlSplitContainer.Panel2.SuspendLayout();
            this.InternalControlSplitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AttributesProcessedCountNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CategorizedObservationsCountNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ForestTreesCountNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ForestTrainingObservationsCountNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InternalGraphSplitContainer)).BeginInit();
            this.InternalGraphSplitContainer.Panel1.SuspendLayout();
            this.InternalGraphSplitContainer.Panel2.SuspendLayout();
            this.InternalGraphSplitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TreeVisualizationSplitContainer)).BeginInit();
            this.TreeVisualizationSplitContainer.Panel1.SuspendLayout();
            this.TreeVisualizationSplitContainer.Panel2.SuspendLayout();
            this.TreeVisualizationSplitContainer.SuspendLayout();
            this.panel1.SuspendLayout();
            this.InformationGainGraphTypePanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TreePicture)).BeginInit();
            this.SuspendLayout();
            // 
            // ChoseDataButton
            // 
            this.ChoseDataButton.Location = new System.Drawing.Point(11, 11);
            this.ChoseDataButton.Name = "ChoseDataButton";
            this.ChoseDataButton.Size = new System.Drawing.Size(162, 38);
            this.ChoseDataButton.TabIndex = 0;
            this.ChoseDataButton.Text = "Wybierz zbiór treningowy";
            this.ChoseDataButton.UseVisualStyleBackColor = true;
            this.ChoseDataButton.Click += new System.EventHandler(this.ChoseDataButton_Click);
            // 
            // ExternalSplitContainer
            // 
            this.ExternalSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ExternalSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.ExternalSplitContainer.Name = "ExternalSplitContainer";
            // 
            // ExternalSplitContainer.Panel1
            // 
            this.ExternalSplitContainer.Panel1.Controls.Add(this.InternalControlSplitContainer);
            // 
            // ExternalSplitContainer.Panel2
            // 
            this.ExternalSplitContainer.Panel2.Controls.Add(this.InternalGraphSplitContainer);
            this.ExternalSplitContainer.Size = new System.Drawing.Size(1189, 682);
            this.ExternalSplitContainer.SplitterDistance = 395;
            this.ExternalSplitContainer.TabIndex = 2;
            // 
            // InternalControlSplitContainer
            // 
            this.InternalControlSplitContainer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.InternalControlSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.InternalControlSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.InternalControlSplitContainer.Name = "InternalControlSplitContainer";
            this.InternalControlSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // InternalControlSplitContainer.Panel1
            // 
            this.InternalControlSplitContainer.Panel1.Controls.Add(this.AttributesProcessedCountNumericUpDown);
            this.InternalControlSplitContainer.Panel1.Controls.Add(this.label6);
            this.InternalControlSplitContainer.Panel1.Controls.Add(this.CategorizedObservationsCountNumericUpDown);
            this.InternalControlSplitContainer.Panel1.Controls.Add(this.label5);
            this.InternalControlSplitContainer.Panel1.Controls.Add(this.CategorizeButton);
            this.InternalControlSplitContainer.Panel1.Controls.Add(this.ForestTreesCountNumericUpDown);
            this.InternalControlSplitContainer.Panel1.Controls.Add(this.label4);
            this.InternalControlSplitContainer.Panel1.Controls.Add(this.ForestTrainingObservationsCountNumericUpDown);
            this.InternalControlSplitContainer.Panel1.Controls.Add(this.label3);
            this.InternalControlSplitContainer.Panel1.Controls.Add(this.GenerateForestsButton);
            this.InternalControlSplitContainer.Panel1.Controls.Add(this.ChoseDataButton);
            // 
            // InternalControlSplitContainer.Panel2
            // 
            this.InternalControlSplitContainer.Panel2.Controls.Add(this.StatisticsLabel);
            this.InternalControlSplitContainer.Panel2.Controls.Add(this.label1);
            this.InternalControlSplitContainer.Size = new System.Drawing.Size(395, 682);
            this.InternalControlSplitContainer.SplitterDistance = 266;
            this.InternalControlSplitContainer.TabIndex = 2;
            // 
            // AttributesProcessedCountNumericUpDown
            // 
            this.AttributesProcessedCountNumericUpDown.Location = new System.Drawing.Point(305, 114);
            this.AttributesProcessedCountNumericUpDown.Name = "AttributesProcessedCountNumericUpDown";
            this.AttributesProcessedCountNumericUpDown.Size = new System.Drawing.Size(73, 20);
            this.AttributesProcessedCountNumericUpDown.TabIndex = 11;
            this.AttributesProcessedCountNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.Location = new System.Drawing.Point(145, 116);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(159, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Ilość procesowanych atrybutów:";
            // 
            // CategorizedObservationsCountNumericUpDown
            // 
            this.CategorizedObservationsCountNumericUpDown.Location = new System.Drawing.Point(174, 182);
            this.CategorizedObservationsCountNumericUpDown.Name = "CategorizedObservationsCountNumericUpDown";
            this.CategorizedObservationsCountNumericUpDown.Size = new System.Drawing.Size(73, 20);
            this.CategorizedObservationsCountNumericUpDown.TabIndex = 9;
            this.CategorizedObservationsCountNumericUpDown.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(145, 164);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(180, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Ilość obserwacji kategoryzowanych: ";
            // 
            // CategorizeButton
            // 
            this.CategorizeButton.Location = new System.Drawing.Point(11, 164);
            this.CategorizeButton.Name = "CategorizeButton";
            this.CategorizeButton.Size = new System.Drawing.Size(95, 38);
            this.CategorizeButton.TabIndex = 7;
            this.CategorizeButton.Text = "Kategoryzuj";
            this.CategorizeButton.UseVisualStyleBackColor = true;
            this.CategorizeButton.Click += new System.EventHandler(this.CategorizeButton_Click);
            // 
            // ForestTreesCountNumericUpDown
            // 
            this.ForestTreesCountNumericUpDown.Location = new System.Drawing.Point(305, 88);
            this.ForestTreesCountNumericUpDown.Name = "ForestTreesCountNumericUpDown";
            this.ForestTreesCountNumericUpDown.Size = new System.Drawing.Size(73, 20);
            this.ForestTreesCountNumericUpDown.TabIndex = 6;
            this.ForestTreesCountNumericUpDown.Value = new decimal(new int[] {
            25,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(145, 90);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(101, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Ilość drzew w lesie: ";
            // 
            // ForestTrainingObservationsCountNumericUpDown
            // 
            this.ForestTrainingObservationsCountNumericUpDown.Location = new System.Drawing.Point(305, 65);
            this.ForestTrainingObservationsCountNumericUpDown.Name = "ForestTrainingObservationsCountNumericUpDown";
            this.ForestTrainingObservationsCountNumericUpDown.Size = new System.Drawing.Size(73, 20);
            this.ForestTrainingObservationsCountNumericUpDown.TabIndex = 4;
            this.ForestTrainingObservationsCountNumericUpDown.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(145, 65);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(154, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Ilość obserwacji treningowych: ";
            // 
            // GenerateForestsButton
            // 
            this.GenerateForestsButton.Location = new System.Drawing.Point(11, 65);
            this.GenerateForestsButton.Name = "GenerateForestsButton";
            this.GenerateForestsButton.Size = new System.Drawing.Size(128, 38);
            this.GenerateForestsButton.TabIndex = 2;
            this.GenerateForestsButton.Text = "Generuj drzewa i lasy losowe";
            this.GenerateForestsButton.UseVisualStyleBackColor = true;
            this.GenerateForestsButton.Click += new System.EventHandler(this.GenerateForestsButton_Click);
            // 
            // StatisticsLabel
            // 
            this.StatisticsLabel.AutoSize = true;
            this.StatisticsLabel.Location = new System.Drawing.Point(11, 28);
            this.StatisticsLabel.Name = "StatisticsLabel";
            this.StatisticsLabel.Size = new System.Drawing.Size(0, 13);
            this.StatisticsLabel.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label1.Location = new System.Drawing.Point(171, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Statystyka";
            // 
            // InternalGraphSplitContainer
            // 
            this.InternalGraphSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.InternalGraphSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.InternalGraphSplitContainer.Name = "InternalGraphSplitContainer";
            this.InternalGraphSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // InternalGraphSplitContainer.Panel1
            // 
            this.InternalGraphSplitContainer.Panel1.Controls.Add(this.TreeVisualizationSplitContainer);
            this.InternalGraphSplitContainer.Panel1.Controls.Add(this.InformationGainGraphTypePanel);
            // 
            // InternalGraphSplitContainer.Panel2
            // 
            this.InternalGraphSplitContainer.Panel2.AutoScroll = true;
            this.InternalGraphSplitContainer.Panel2.Controls.Add(this.TreePicture);
            this.InternalGraphSplitContainer.Panel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.InternalGraphSplitContainer.Size = new System.Drawing.Size(790, 682);
            this.InternalGraphSplitContainer.SplitterDistance = 92;
            this.InternalGraphSplitContainer.TabIndex = 0;
            // 
            // TreeVisualizationSplitContainer
            // 
            this.TreeVisualizationSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TreeVisualizationSplitContainer.Location = new System.Drawing.Point(167, 0);
            this.TreeVisualizationSplitContainer.Name = "TreeVisualizationSplitContainer";
            // 
            // TreeVisualizationSplitContainer.Panel1
            // 
            this.TreeVisualizationSplitContainer.Panel1.Controls.Add(this.label2);
            // 
            // TreeVisualizationSplitContainer.Panel2
            // 
            this.TreeVisualizationSplitContainer.Panel2.Controls.Add(this.DisplayTreeButton);
            this.TreeVisualizationSplitContainer.Panel2.Controls.Add(this.panel1);
            this.TreeVisualizationSplitContainer.Panel2.Controls.Add(this.DrawnTreeIndexLabel);
            this.TreeVisualizationSplitContainer.Panel2.Controls.Add(this.NextTreeButton);
            this.TreeVisualizationSplitContainer.Panel2.Controls.Add(this.PreviousTreeButton);
            this.TreeVisualizationSplitContainer.Size = new System.Drawing.Size(623, 92);
            this.TreeVisualizationSplitContainer.SplitterDistance = 160;
            this.TreeVisualizationSplitContainer.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(23, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(114, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Podgląd drzew";
            // 
            // DisplayTreeButton
            // 
            this.DisplayTreeButton.Location = new System.Drawing.Point(156, 10);
            this.DisplayTreeButton.Name = "DisplayTreeButton";
            this.DisplayTreeButton.Size = new System.Drawing.Size(127, 23);
            this.DisplayTreeButton.TabIndex = 4;
            this.DisplayTreeButton.Text = "Wyświetl drzewo";
            this.DisplayTreeButton.UseVisualStyleBackColor = true;
            this.DisplayTreeButton.Click += new System.EventHandler(this.DisplayTreeButton_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.PrunningDisabledRadioButton);
            this.panel1.Controls.Add(this.PrunningEnalbedradioButton);
            this.panel1.Location = new System.Drawing.Point(4, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(146, 57);
            this.panel1.TabIndex = 3;
            // 
            // PrunningDisabledRadioButton
            // 
            this.PrunningDisabledRadioButton.AutoSize = true;
            this.PrunningDisabledRadioButton.Checked = true;
            this.PrunningDisabledRadioButton.Location = new System.Drawing.Point(4, 37);
            this.PrunningDisabledRadioButton.Name = "PrunningDisabledRadioButton";
            this.PrunningDisabledRadioButton.Size = new System.Drawing.Size(103, 17);
            this.PrunningDisabledRadioButton.TabIndex = 1;
            this.PrunningDisabledRadioButton.TabStop = true;
            this.PrunningDisabledRadioButton.Text = "Brak przycinania";
            this.PrunningDisabledRadioButton.UseVisualStyleBackColor = true;
            // 
            // PrunningEnalbedradioButton
            // 
            this.PrunningEnalbedradioButton.AutoSize = true;
            this.PrunningEnalbedradioButton.Location = new System.Drawing.Point(4, 14);
            this.PrunningEnalbedradioButton.Name = "PrunningEnalbedradioButton";
            this.PrunningEnalbedradioButton.Size = new System.Drawing.Size(79, 17);
            this.PrunningEnalbedradioButton.TabIndex = 0;
            this.PrunningEnalbedradioButton.Text = "Przycinanie";
            this.PrunningEnalbedradioButton.UseVisualStyleBackColor = true;
            this.PrunningEnalbedradioButton.CheckedChanged += new System.EventHandler(this.PrunningEnalbedradioButton_CheckedChanged);
            // 
            // DrawnTreeIndexLabel
            // 
            this.DrawnTreeIndexLabel.AutoSize = true;
            this.DrawnTreeIndexLabel.Location = new System.Drawing.Point(292, 45);
            this.DrawnTreeIndexLabel.Name = "DrawnTreeIndexLabel";
            this.DrawnTreeIndexLabel.Size = new System.Drawing.Size(0, 13);
            this.DrawnTreeIndexLabel.TabIndex = 2;
            // 
            // NextTreeButton
            // 
            this.NextTreeButton.Location = new System.Drawing.Point(353, 40);
            this.NextTreeButton.Name = "NextTreeButton";
            this.NextTreeButton.Size = new System.Drawing.Size(75, 23);
            this.NextTreeButton.TabIndex = 1;
            this.NextTreeButton.Text = ">";
            this.NextTreeButton.UseVisualStyleBackColor = true;
            this.NextTreeButton.Click += new System.EventHandler(this.NextTreeButton_Click);
            // 
            // PreviousTreeButton
            // 
            this.PreviousTreeButton.Location = new System.Drawing.Point(194, 39);
            this.PreviousTreeButton.Name = "PreviousTreeButton";
            this.PreviousTreeButton.Size = new System.Drawing.Size(75, 23);
            this.PreviousTreeButton.TabIndex = 0;
            this.PreviousTreeButton.Text = "<";
            this.PreviousTreeButton.UseVisualStyleBackColor = true;
            this.PreviousTreeButton.Click += new System.EventHandler(this.PreviousTreeButton_Click);
            // 
            // InformationGainGraphTypePanel
            // 
            this.InformationGainGraphTypePanel.Controls.Add(this.GiniGraphRadioButton);
            this.InformationGainGraphTypePanel.Controls.Add(this.EntropyGraphRadioButton);
            this.InformationGainGraphTypePanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.InformationGainGraphTypePanel.Location = new System.Drawing.Point(0, 0);
            this.InformationGainGraphTypePanel.Name = "InformationGainGraphTypePanel";
            this.InformationGainGraphTypePanel.Size = new System.Drawing.Size(167, 92);
            this.InformationGainGraphTypePanel.TabIndex = 3;
            // 
            // GiniGraphRadioButton
            // 
            this.GiniGraphRadioButton.AutoSize = true;
            this.GiniGraphRadioButton.Location = new System.Drawing.Point(35, 43);
            this.GiniGraphRadioButton.Name = "GiniGraphRadioButton";
            this.GiniGraphRadioButton.Size = new System.Drawing.Size(132, 17);
            this.GiniGraphRadioButton.TabIndex = 1;
            this.GiniGraphRadioButton.Text = "Wspołczynnik Giniego";
            this.GiniGraphRadioButton.UseVisualStyleBackColor = true;
            this.GiniGraphRadioButton.CheckedChanged += new System.EventHandler(this.GiniGraphRadioButton_CheckedChanged);
            // 
            // EntropyGraphRadioButton
            // 
            this.EntropyGraphRadioButton.AutoSize = true;
            this.EntropyGraphRadioButton.Checked = true;
            this.EntropyGraphRadioButton.Location = new System.Drawing.Point(35, 13);
            this.EntropyGraphRadioButton.Name = "EntropyGraphRadioButton";
            this.EntropyGraphRadioButton.Size = new System.Drawing.Size(64, 17);
            this.EntropyGraphRadioButton.TabIndex = 0;
            this.EntropyGraphRadioButton.TabStop = true;
            this.EntropyGraphRadioButton.Text = "Entropia";
            this.EntropyGraphRadioButton.UseVisualStyleBackColor = true;
            this.EntropyGraphRadioButton.CheckedChanged += new System.EventHandler(this.EntropyGraphRadioButton_CheckedChanged);
            // 
            // TreePicture
            // 
            this.TreePicture.Location = new System.Drawing.Point(3, 3);
            this.TreePicture.Name = "TreePicture";
            this.TreePicture.Size = new System.Drawing.Size(100, 50);
            this.TreePicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.TreePicture.TabIndex = 0;
            this.TreePicture.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1189, 682);
            this.Controls.Add(this.ExternalSplitContainer);
            this.Name = "Form1";
            this.Text = "Form1";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ExternalSplitContainer.Panel1.ResumeLayout(false);
            this.ExternalSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ExternalSplitContainer)).EndInit();
            this.ExternalSplitContainer.ResumeLayout(false);
            this.InternalControlSplitContainer.Panel1.ResumeLayout(false);
            this.InternalControlSplitContainer.Panel1.PerformLayout();
            this.InternalControlSplitContainer.Panel2.ResumeLayout(false);
            this.InternalControlSplitContainer.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.InternalControlSplitContainer)).EndInit();
            this.InternalControlSplitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.AttributesProcessedCountNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CategorizedObservationsCountNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ForestTreesCountNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ForestTrainingObservationsCountNumericUpDown)).EndInit();
            this.InternalGraphSplitContainer.Panel1.ResumeLayout(false);
            this.InternalGraphSplitContainer.Panel2.ResumeLayout(false);
            this.InternalGraphSplitContainer.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.InternalGraphSplitContainer)).EndInit();
            this.InternalGraphSplitContainer.ResumeLayout(false);
            this.TreeVisualizationSplitContainer.Panel1.ResumeLayout(false);
            this.TreeVisualizationSplitContainer.Panel1.PerformLayout();
            this.TreeVisualizationSplitContainer.Panel2.ResumeLayout(false);
            this.TreeVisualizationSplitContainer.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TreeVisualizationSplitContainer)).EndInit();
            this.TreeVisualizationSplitContainer.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.InformationGainGraphTypePanel.ResumeLayout(false);
            this.InformationGainGraphTypePanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TreePicture)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button ChoseDataButton;
        private System.Windows.Forms.SplitContainer ExternalSplitContainer;
        private System.Windows.Forms.SplitContainer InternalControlSplitContainer;
        private System.Windows.Forms.SplitContainer InternalGraphSplitContainer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel InformationGainGraphTypePanel;
        private System.Windows.Forms.Button GenerateForestsButton;
        private System.Windows.Forms.NumericUpDown ForestTrainingObservationsCountNumericUpDown;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown CategorizedObservationsCountNumericUpDown;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button CategorizeButton;
        private System.Windows.Forms.Label StatisticsLabel;
        private System.Windows.Forms.SplitContainer TreeVisualizationSplitContainer;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button NextTreeButton;
        private System.Windows.Forms.Button PreviousTreeButton;
        public System.Windows.Forms.RadioButton GiniGraphRadioButton;
        public System.Windows.Forms.RadioButton EntropyGraphRadioButton;
        public System.Windows.Forms.PictureBox TreePicture;
        public System.Windows.Forms.Label DrawnTreeIndexLabel;
        public System.Windows.Forms.NumericUpDown ForestTreesCountNumericUpDown;
        private System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.RadioButton PrunningEnalbedradioButton;
        public System.Windows.Forms.RadioButton PrunningDisabledRadioButton;
        public System.Windows.Forms.Button DisplayTreeButton;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown AttributesProcessedCountNumericUpDown;
    }
}

