﻿namespace MSI2Project
{
    using System.Collections.Generic;
    using System.Globalization;

    public class Tree
    {
        public TreeNode root { get; private set; }

        public IList<Instance> trainingSet { get; private set; }

        public Tree(TreeNode root, IList<Instance> trainingSet)
        {
            this.root = root;
            this.trainingSet = trainingSet;
        }

        public string CategorizeObservation(Instance observation)
        {
            var node = root;
            while (node.Category == null)
            {
                switch (DataLayer.AttributesTypes[node.SplitAttributeIndex])
                {
                    case AttributeType.Text:
                        {
                            node = node.Children[observation.attributes[node.SplitAttributeIndex]];
                            continue;
                        }
                    case AttributeType.Numeric:
                        {
                            if (double.Parse(observation.attributes[node.SplitAttributeIndex], CultureInfo.InvariantCulture) < node.SplitValue)
                            {
                                node = node.Children[string.Format("<{0}", node.SplitValue)];
                                continue;
                            }
                            else
                            {
                                node = node.Children[string.Format(">={0}", node.SplitValue)];
                                continue;
                            }
                        }
                }
            }
            return node.Category;
        }
    }
}
