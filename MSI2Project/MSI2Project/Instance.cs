﻿namespace MSI2Project
{
    public class Instance
    {
        public string[] attributes;

        public string category;

        public Instance(string[] values)
        {
            attributes = new string[values.Length - 1];
            for (int i = 0; i < values.Length - 1; i++)
            {
                attributes[i] = values[i];
            }

            category = values[values.Length - 1];
        }
    }
}
