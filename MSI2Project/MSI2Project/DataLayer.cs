﻿namespace MSI2Project
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;

    public static class DataLayer
    {
        public const string UnknownAttributeValue = "?";

        public static Instance[] Instances { get; private set; }

        public static IList<string>[] AllowedAttributesValues { get; private set; }

        public static IList<string> Categories { get; private set; }

        public static AttributeType[] AttributesTypes { get; set; }

        private static Random RandomGenerator = new Random();


        public static void ParseData(StreamReader fileStream)
        {
            var instances = new List<Instance>();
            string line;
            while ((line = fileStream.ReadLine()) != null && !String.IsNullOrEmpty(line))
            {
                Dictionary<int, string> instanceAttributes = new Dictionary<int, string>();
                var values = line.Split(',');
                string category = values.Last();

                var instance = new Instance(values);

                instances.Add(instance);
            }
            var selector = new AttributesTypesSelector(instances[0].attributes.Count());
            selector.ShowDialog();

            Instances = instances.OrderBy(a => RandomGenerator.Next()).ToArray();
            storeAttributeAllowedValues();
            countPossibleCategories();
        }

        /// <summary>
        /// Returns random "amount" Instances from observations set
        /// </summary>
        /// <param name="amount"></param>
        /// <returns></returns>
        public static IList<Instance> GetRandomSetOfInstances(int amount)
        {
            var randomSet = new List<Instance>();
            for(int i = 1 ; i <= amount ; i++)
            {
                randomSet.Add(Instances[RandomGenerator.Next(Instances.Count())]);
            }

            return randomSet;
        }

        /// <summary>
        /// Returns category which occured in provided set most times.
        /// </summary>
        /// <param name="instances"></param>
        /// <returns></returns>
        public static string GetMostPopularCategoryInSet(IList<Instance> instances)
        {
            var counter = new Dictionary<string, int>();

            foreach(var instance in instances)
            {
                var category = instance.category;

                if(!counter.ContainsKey(category))
                {
                    counter.Add(category, 1);
                }
                else
                {
                    counter[category]++;
                }
            }

             return counter.Aggregate((l, r) => l.Value > r.Value ? l : r).Key;
        }

        /// <summary>
        /// Returns what part of set is in each category 
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public static Dictionary<string, double> GetCategoriesPopularityInSet_General(IList<Instance> instancesSet)
        {
            var retValue = new Dictionary<string, double>();

            foreach(var instance in instancesSet)
            {
                if(retValue.ContainsKey(instance.category))
                {
                    retValue[instance.category] += 1f / instancesSet.Count;
                }
                else
                {
                    retValue.Add(instance.category, 1f / instancesSet.Count);
                }
            }

            return retValue;
        }

        /// <summary>
        /// Returns what part of set is in each category. Taking into consideration only instances with value attributeValue in attribute attributeIndex.
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public static Dictionary<string, double> GetCategoriesPopularityInSet_Attribute(string attributeValue, int attributeIndex, IList<Instance> instancesSet)
        {
            var retValue = new Dictionary<string, double>();
            var instancesSet_Attribute = instancesSet.Where(instance => instance.attributes[attributeIndex] == attributeValue);

            foreach (var instance in instancesSet_Attribute)
            {
                if (retValue.ContainsKey(instance.category))
                {
                    retValue[instance.category] += 1f / instancesSet_Attribute.Count();
                }
                else
                {
                    retValue.Add(instance.category, 1f / instancesSet_Attribute.Count());
                }
            }

            return retValue;
        }

        /// <summary>
        /// Return number of instances that have attributeValue in attribute with attributeIndex
        /// </summary>
        /// <param name="attributeIndex"></param>
        /// <param name="attributeValue"></param>
        /// <param name="instancesSet"></param>
        /// <returns></returns>
        public static int GetAmountOfInstancesWithAttrbuteValue(int attributeIndex, string attributeValue, IList<Instance> instancesSet)
        {
            return instancesSet.Count(instance => instance.attributes[attributeIndex] == attributeValue);
        }

        public static List<int> GetRandomAttributesIndexesAsList(int amount)
        {
            var retList = new List<int>();
            

            for(int i = 0 ; i < Instances[1].attributes.Count() ; i++ )
            {
                retList.Add(i);
            }
            var m = retList.OrderBy(x => RandomGenerator.Next());

            return retList.OrderBy(x => RandomGenerator.Next()).Take(amount).ToList(); 
        }

        public static IList<double> GetNumericAttributeValuesInSet(IList<Instance> instances, int attribute)
        {
            var counter = new List<double>();

            foreach (var instance in instances)
            {
                if (instance.attributes[attribute] != DataLayer.UnknownAttributeValue && !counter.Contains(double.Parse(instance.attributes[attribute], CultureInfo.InvariantCulture)))
                {
                    counter.Add(double.Parse(instance.attributes[attribute], CultureInfo.InvariantCulture));
                }
            }

            return counter;
        }

        public static int[] GetGroupsCountsAfterSetSplitByNumericValue(IList<Instance> instances, int attribute, double attributeSplitValue)
        {
            var retVal = new int[2];

            retVal[0] = instances.Where(instance => double.Parse(instance.attributes[attribute], CultureInfo.InvariantCulture) < attributeSplitValue).Count();
            retVal[1] = instances.Where(instance => double.Parse(instance.attributes[attribute], CultureInfo.InvariantCulture) >= attributeSplitValue).Count();

            return retVal;
        }

        public static IList<Instance>[] GetGroupsAfterSetSplitByNumericValue(IList<Instance> instances, int attribute, double attributeSplitValue)
        {
            var retVal = new List<Instance>[2];

            retVal[0] = instances.Where(instance => double.Parse(instance.attributes[attribute], CultureInfo.InvariantCulture) < attributeSplitValue).ToList();
            retVal[1] = instances.Where(instance => double.Parse(instance.attributes[attribute], CultureInfo.InvariantCulture) >= attributeSplitValue).ToList();

            return retVal;
        }

        private static void storeAttributeAllowedValues()
        {
            AllowedAttributesValues = new IList<string>[Instances[0].attributes.Length];

            for (int i = 0; i < Instances[0].attributes.Length; i++)
            {
                AllowedAttributesValues[i] = new List<string>();
            }

            foreach (var instance in Instances)
            {
                for (int i = 0; i < instance.attributes.Count(); i++)
                {
                    if(!AllowedAttributesValues[i].Contains(instance.attributes[i]) && instance.attributes[i] != DataLayer.UnknownAttributeValue)
                    {
                        AllowedAttributesValues[i].Add(instance.attributes[i]);
                    }
                }
            }
        }

        private static void countPossibleCategories()
        {
            Categories = new List<string>();
            foreach (var instance in Instances)
            {
               if(Categories.Contains(instance.category))
               {
                   continue;
               }

               Categories.Add(instance.category);
            }
        }
    }
}