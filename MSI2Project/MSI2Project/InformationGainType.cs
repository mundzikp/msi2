﻿namespace MSI2Project
{
    public enum InformationGainType
    {
        Entropy = 1,
        GiniIndex = 2
    }
}
