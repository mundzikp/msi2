﻿namespace MSI2Project
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;

    public class InformationGainCalculator
    {

        #region Entropy

        public InformationGain Entropy(int attributeIndex, IList<Instance> instances, IList<int> attributes)
        {
            InformationGain retVal;

            switch (DataLayer.AttributesTypes[attributeIndex])
            {
                case AttributeType.Text:
                    {
                        this.Entropy_Text(attributeIndex, instances, attributes, out retVal);
                        return retVal;
                    }
                case AttributeType.Numeric:
                    {
                        this.Entropy_Numeric(attributeIndex, instances, attributes, out retVal);
                        return retVal;
                    }
            }

            return null;
        }

        private void Entropy_Text(int attributeIndex, IList<Instance> instances, IList<int> attributes, out InformationGain retVal)
        {
            retVal = new InformationGain();
            retVal.attributeIndex = attributeIndex;
            retVal.attributeType = AttributeType.Text;

            retVal.gainValue = -DataLayer.GetCategoriesPopularityInSet_General(instances).Sum(p => p.Value * Math.Log(p.Value, 2)) - E_Text(attributeIndex, instances);
        }

        private double E_Text(int attributeIndex, IList<Instance> instances)
        {
            var retVal = 0d;

            foreach (var attributeValue in DataLayer.AllowedAttributesValues[attributeIndex])
            {
                retVal += (DataLayer.GetAmountOfInstancesWithAttrbuteValue(attributeIndex, attributeValue, instances) / (double)instances.Count)
                    * (-DataLayer.GetCategoriesPopularityInSet_Attribute(attributeValue, attributeIndex, instances).Sum(p => p.Value * Math.Log(p.Value, 2)));
            }
            return retVal;
        }

        private void Entropy_Numeric(int attributeIndex, IList<Instance> instances, IList<int> attributes, out InformationGain retVal)
        {
            double bestValue = double.MinValue;
            retVal = new InformationGain();
            retVal.attributeIndex = attributeIndex;
            retVal.attributeType = AttributeType.Numeric;

            var popularityInSetGeneral = -DataLayer.GetCategoriesPopularityInSet_General(instances).Sum(p => p.Value * Math.Log(p.Value, 2));


            foreach (var attributeValue in DataLayer.GetNumericAttributeValuesInSet(instances, attributeIndex))
            {
                var groupsCounts = DataLayer.GetGroupsCountsAfterSetSplitByNumericValue(instances, attributeIndex, attributeValue);
                var popularityInSetAttribute1 = groupsCounts[0] / (double)instances.Count;
                var popularityInSetAttribute2 = groupsCounts[1] / (double)instances.Count;
                var currentValue =
                    popularityInSetGeneral
                    - popularityInSetAttribute1 * Math.Log(popularityInSetAttribute1, 2)
                    - popularityInSetAttribute2 * Math.Log(popularityInSetAttribute2, 2);

                if (bestValue < currentValue)
                {
                    bestValue = currentValue;
                    retVal.gainValue = bestValue;
                    retVal.splitAttributeValue = attributeValue;
                }
            }
        }

        #endregion

        #region GiniIndex

        //We use negative value to preserve looking for maximum
        public InformationGain Neg_GiniIndex(int attributeIndex, IList<Instance> instances, IList<int> attributes)
        {
            InformationGain retVal;

            switch (DataLayer.AttributesTypes[attributeIndex])
            {
                case AttributeType.Text:
                    {
                        this.Neg_GiniIndex_Text(attributeIndex, instances, attributes, out retVal);
                        return retVal;
                    }
                case AttributeType.Numeric:
                    {
                        this.Neg_GiniIndex_Numeric(attributeIndex, instances, attributes, out retVal);
                        return retVal;
                    }
            }

            return null;
        }

        private void Neg_GiniIndex_Text(int attributeIndex, IList<Instance> instances, IList<int> attributes, out InformationGain retVal)
        {
            retVal = new InformationGain();
            retVal.attributeIndex = attributeIndex;
            retVal.attributeType = AttributeType.Text;

            foreach (var attributeValue in DataLayer.AllowedAttributesValues[attributeIndex])
            {
                var Si = instances.Where(instance => instance.attributes[attributeIndex] == attributeValue);
                retVal.gainValue += Si.Count() / (double)instances.Count * Gini(Si.ToList());
            }

            retVal.gainValue = -retVal.gainValue;
        }

        private void Neg_GiniIndex_Numeric(int attributeIndex, IList<Instance> instances, IList<int> attributes, out InformationGain retVal)
        {
            double bestValue = double.MinValue;
            retVal = new InformationGain();
            retVal.attributeIndex = attributeIndex;
            retVal.attributeType = AttributeType.Numeric;
             
            foreach (var attributeValue in DataLayer.GetNumericAttributeValuesInSet(instances, attributeIndex))
            {
                var groups = DataLayer.GetGroupsAfterSetSplitByNumericValue(instances, attributeIndex, attributeValue);
                var currentValue =
                    groups[0].Count / (double)instances.Count * Gini(groups[0]) +
                    groups[1].Count / (double)instances.Count * Gini(groups[1]);

                if (bestValue < -currentValue)
                {
                    bestValue = -currentValue;
                    retVal.gainValue = bestValue;
                    retVal.splitAttributeValue = attributeValue;
                }
            }
        }

        private double Gini(IList<Instance> instances)
        {
            if (instances == null || instances.Count == 0)
            {
                return 1;
            }

            return 1 - DataLayer.GetCategoriesPopularityInSet_General(instances).Sum(instance => Math.Pow(instance.Value, 2d));
        }
    }

        #endregion

}
