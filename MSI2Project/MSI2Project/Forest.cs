﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSI2Project
{
    public class Forest
    {
        public InformationGainType TreeType { get; private set; }

        public IList<Tree> Trees { get; private set; }

        public Forest(InformationGainType treeType, IList<Tree> treesList)
        {
            this.TreeType = treeType;
            this.Trees = treesList;
        }

        public string CategorizeObservation(Instance observation)
        {
            List<string> results=new List<string>();
            foreach (var tree in Trees)
            {
                results.Add(tree.CategorizeObservation(observation));
            }
            return results.GroupBy(i => i).OrderByDescending(grp => grp.Count()).Select(grp => grp.Key).First();
        }
    }
}