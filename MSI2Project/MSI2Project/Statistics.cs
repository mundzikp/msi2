﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace MSI2Project
{
    public class Statistics
    {
        public int CorrectForestCategorizations { get; set; }
        public int WrongForestCategorizations { get; set; }
        public double ForestCategorizationsEfficiency { get; set; }
        public int CorrectTreeCategorizations { get; set; }
        public int WrongTreeCategorizations { get; set; }
        public double TreeCategorizationEfficiency { get; set; }

        public Statistics(Forest forest, IList<Instance> observationToCalssify)
        {
            List<Tuple<string, string>> resultsComparison = new List<Tuple<string, string>>();
            foreach (var observation in observationToCalssify)
            {
                string properCategory = observation.category;

                resultsComparison.Add(new Tuple<string, string>(properCategory,
                    forest.CategorizeObservation(observation)));
            }
            foreach (var tuple in resultsComparison)
            {
                if (tuple.Item1.Equals(tuple.Item2))
                {
                    CorrectForestCategorizations++;
                }
                else
                {
                    WrongForestCategorizations++;
                }
            }
            ForestCategorizationsEfficiency =
                Math.Round(
                    (double) CorrectForestCategorizations / (CorrectForestCategorizations + WrongForestCategorizations) *
                    100, 2);
        }

        public Statistics(Tree tree, IList<Instance> observationToCalssify)
        {
            List<Tuple<string, string>> resultsComparison = new List<Tuple<string, string>>();
            foreach (var observation in observationToCalssify)
            {
                string properCategory = observation.category;

                resultsComparison.Add(new Tuple<string, string>(properCategory,
                    tree.CategorizeObservation(observation)));
            }
            foreach (var tuple in resultsComparison)
            {
                if (tuple.Item1.Equals(tuple.Item2))
                {
                    CorrectTreeCategorizations++;
                }
                else
                {
                    WrongTreeCategorizations++;
                }
            }
            TreeCategorizationEfficiency =
                Math.Round(
                    (double)CorrectTreeCategorizations / (CorrectTreeCategorizations + WrongTreeCategorizations) *
                    100, 2);
        }

        public void SetStatisticsLabelForForests(InformationGainType informationGainType,
            System.Windows.Forms.Label statisticsLabel)
        {
            string correctCategorizationsText =
                string.Format("[las - {0}]: Poprawnie skategoryzowane obserwacje: {1}/{2}, frakcja: {3}%",
                    informationGainType.ToString(), CorrectForestCategorizations,
                    CorrectForestCategorizations + WrongForestCategorizations, ForestCategorizationsEfficiency);
            string wrongCategorizationsText =
                string.Format("[las - {0}]: Niepoprawnie skategoryzowane obserwacje: {1}/{2}, frakcja: {3}%",
                    informationGainType.ToString(), WrongForestCategorizations,
                    CorrectForestCategorizations + WrongForestCategorizations, 100 - ForestCategorizationsEfficiency);

            statisticsLabel.Font= new Font("Arial", 12);
            statisticsLabel.Text += Environment.NewLine + correctCategorizationsText + Environment.NewLine +
                                    wrongCategorizationsText + Environment.NewLine;
        }

        public void SetStatisticsLabelForTree(InformationGainType informationGainType,
          System.Windows.Forms.Label statisticsLabel)
        {
            string correctCategorizationsText =
                string.Format("[drzewo - {0}]: Poprawnie skategoryzowane obserwacje: {1}/{2}, frakcja: {3}%",
                    informationGainType.ToString(), CorrectTreeCategorizations,
                    CorrectTreeCategorizations + WrongTreeCategorizations, TreeCategorizationEfficiency);
            string wrongCategorizationsText =
                string.Format("[drzewo - {0}]: Niepoprawnie skategoryzowane obserwacje: {1}/{2}, frakcja: {3}%",
                    informationGainType.ToString(), WrongTreeCategorizations,
                    CorrectTreeCategorizations + WrongTreeCategorizations, 100 - TreeCategorizationEfficiency);

            statisticsLabel.Font = new Font("Arial", 12);
            statisticsLabel.Text += Environment.NewLine + correctCategorizationsText + Environment.NewLine +
                                    wrongCategorizationsText + Environment.NewLine;
        }
    }
}