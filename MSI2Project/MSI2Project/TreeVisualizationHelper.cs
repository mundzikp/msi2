﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Management.Instrumentation;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MSI2Project
{
    public class TreeVisualizationHelper
    {
        private string dotExePath = "C:\\Program Files (x86)\\Graphviz2.38\\bin\\dot.exe";
        private Form1 mainForm;
        private int activeTreeIndex;
        private Tree activeTree;
        private Image[] readyEntropyTreeImages;
        private Image[] readyGiniTreeImages;
        private Image readyEntropyFullTreeImage;
        private Image readyGiniIndexFullTreeImage;

        public TreeVisualizationHelper(Form1 mainForm)
        {
            this.mainForm = mainForm;
            this.activeTreeIndex = 0;
            this.activeTree = mainForm.EntropyGraphRadioButton.Checked
                ? TreeLayer.EntropyForest.Trees.First()
                : TreeLayer.GiniIndexForest.Trees.First();

            this.readyEntropyTreeImages = new Image[(int) mainForm.ForestTreesCountNumericUpDown.Value];
            this.readyGiniTreeImages = new Image[(int) mainForm.ForestTreesCountNumericUpDown.Value];

            SetDrawnTreeLabel();
        }

        #region Recursive actions

        private int nodeIndex = 0;

        private void TreeNodesWalker(TreeNode node, StringBuilder stringBuilder, int currentNodeIndex)
        {
            foreach (var child in node.Children)
            {
                if (child.Value.Category != null)
                {
                    int categoryChildIdx = ++nodeIndex;
                    stringBuilder.AppendLine(string.Format("{0} -> {1}[label=\"{2}\",weight=\"{2}\"];",
                        currentNodeIndex.ToString(), categoryChildIdx.ToString(), child.Key));
                    stringBuilder.AppendLine(
                        string.Format(
                            "{0}[label = \"{1}\", shape = ellipse, fillcolor = \"burlywood\", style = \"filled\"];",
                            categoryChildIdx.ToString(), child.Value.Category));
                }
                else
                {
                    int notCategoryChildIdx = ++nodeIndex;
                    TreeNodesWalker(child.Value, stringBuilder, notCategoryChildIdx);

                    stringBuilder.AppendLine(string.Format("{0} -> {1}[label=\"{2}\",weight=\"{2}\"];",
                        currentNodeIndex.ToString(), notCategoryChildIdx.ToString(), child.Key));
                }
            }
            stringBuilder.AppendLine(
                string.Format("{0}[label = \"{1}\"];", currentNodeIndex.ToString(),
                    "indeks atrybutu dzielącego: " + node.SplitAttributeIndex.ToString() + Environment.NewLine +
                    "zysk informacji: " + Math.Round(node.SplitAttributeGainInformationValue, 2).ToString()));
        }

        private string CreateTreeDotRepresentation(Tree tree)
        {
            var stringBuilder = new StringBuilder();

            stringBuilder.AppendLine("digraph graphname {");

            //recursive call for tree nodes
            TreeNodesWalker(tree.root, stringBuilder, nodeIndex);
            stringBuilder.AppendLine("}");

            return stringBuilder.ToString();
        }

        private string createDotFilePath(bool isFullTree)
        {
            return Path.Combine(Directory.GetCurrentDirectory(), string.Format("{0}tree{1}.dot", mainForm.EntropyGraphRadioButton.Checked ? "e" : "g", isFullTree ? "full" : activeTreeIndex.ToString()));
        }

        private string createPngFilePath(bool isFullTree)
        {
            return Path.Combine(Directory.GetCurrentDirectory(), string.Format("{0}tree{1}.png", mainForm.EntropyGraphRadioButton.Checked ? "e" : "g", isFullTree ? "full" : activeTreeIndex.ToString()));
        }

        private void CreateDotFile(bool isFullTree)
        {
            try
            {
                // Create the file.
                FileStream fs = File.Create(createDotFilePath(isFullTree));
                Byte[] info = new UTF8Encoding(true).GetBytes(CreateTreeDotRepresentation(activeTree));
                fs.Write(info, 0, info.Length);
                fs.Close();
                fs.Dispose();
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        #endregion

        private void CallGraphVizProcess(bool isFullTree)
        {
            var process = new Process();
            process.StartInfo = new ProcessStartInfo(dotExePath)
            {
                Arguments = string.Format("-Tpng \"{0}\" -o \"{1}\"", createDotFilePath(isFullTree), createPngFilePath(isFullTree)),
                UseShellExecute = false,
                CreateNoWindow = true
            };
            process.Start();
            process.WaitForExit();
            process.Close();
            process.Dispose();
        }

        private void SetDrawnTreeLabel()
        {
            mainForm.DrawnTreeIndexLabel.Text = string.Format("{0}/{1}", activeTreeIndex + 1,
                mainForm.ForestTreesCountNumericUpDown.Value);
        }

        protected bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None);
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }
            //file is not locked
            return false;
        }

        public void SetTreeImage()
        {
            activeTree = mainForm.EntropyGraphRadioButton.Checked
                ? TreeLayer.EntropyForest.Trees.ToArray()[activeTreeIndex]
                : TreeLayer.GiniIndexForest.Trees.ToArray()[activeTreeIndex];

            Image image = mainForm.EntropyGraphRadioButton.Checked
                ? readyEntropyTreeImages[activeTreeIndex]
                : readyGiniTreeImages[activeTreeIndex];
            if (image == null)
            {
                CreateDotFile(false);
                CallGraphVizProcess(false);
                //while (IsFileLocked(new FileInfo(createPngFilePath())))
                //    Thread.Sleep(100);
                while (true)
                {
                    try
                    {
                        using (FileStream stream = new FileStream(createPngFilePath(false), FileMode.Open, FileAccess.Read))
                        {
                            image = Image.FromStream(stream);
                            stream.Close();
                            stream.Dispose();
                        }
                        break;
                    }
                    catch (Exception e)
                    {
                        continue;
                    }
                   
                }

                if (mainForm.EntropyGraphRadioButton.Checked)
                    readyEntropyTreeImages[activeTreeIndex] = image;
                else
                    readyGiniTreeImages[activeTreeIndex] = image;
            }

            mainForm.TreePicture.Image = image;
        }

        public void SetFullTreeImage()
        {
            activeTree = mainForm.EntropyGraphRadioButton.Checked
                ? TreeLayer.EntropyFullTree
                : TreeLayer.GiniIndexFullTree;

            Image image = mainForm.EntropyGraphRadioButton.Checked
                ? readyEntropyFullTreeImage
                : readyGiniIndexFullTreeImage;
            if (image == null)
            {
                CreateDotFile(true);
                CallGraphVizProcess(true);
                //while (IsFileLocked(new FileInfo(createPngFilePath())))
                //    Thread.Sleep(100);
                while (true)
                {
                    try
                    {
                        using (FileStream stream = new FileStream(createPngFilePath(true), FileMode.Open, FileAccess.Read))
                        {
                            image = Image.FromStream(stream);
                            stream.Close();
                            stream.Dispose();
                        }
                        break;
                    }
                    catch (Exception e)
                    {
                        continue;
                    }

                }

                if (mainForm.EntropyGraphRadioButton.Checked)
                    readyEntropyFullTreeImage = image;
                else
                    readyGiniIndexFullTreeImage = image;
            }

            mainForm.TreePicture.Image = image;
        }

        public void SetNextTreeImage()
        {
            if (activeTreeIndex >= mainForm.ForestTreesCountNumericUpDown.Value - 1)
                return;

            activeTreeIndex++;

            SetTreeImage();
            SetDrawnTreeLabel();
        }

        public void SetPreviousTreeImage()
        {
            if (activeTreeIndex <= 0)
                return;

            activeTreeIndex--;

            SetTreeImage();
            SetDrawnTreeLabel();
        }
    }
}