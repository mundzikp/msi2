﻿using System.Collections.Generic;
using System.Linq;

namespace MSI2Project
{
    public static class TreeLayer
    {
        public static int ForestTreesCount { get; set; }

        public static int ForestTrainingObservationsCount { get; set; }

        public static int AttributesToUseCount { get; set; }

        public static Forest EntropyForest { get; set; }

        public static Forest GiniIndexForest { get; set; }

        public static Tree EntropyFullTree { get; set; }

        public static Tree GiniIndexFullTree { get; set; }

        public static bool ShouldPrune { get; set; }

        private static Tree[] TreesByEntropy { get; set; }

        private static Tree[] TreesByGiniIndex { get; set; }

        private static TreeConstructor treeConstructor = new TreeConstructor();

        public static void GenerateEntropyForest()
        {
            TreesByEntropy = new Tree[TreeLayer.ForestTreesCount];
            for (int i = 0; i < ForestTreesCount; i++)
            {
                TreesByEntropy[i] = treeConstructor.ConstructTree(InformationGainType.Entropy,
                    DataLayer.GetRandomSetOfInstances(TreeLayer.ForestTrainingObservationsCount),
                    DataLayer.GetRandomSetOfInstances(TreeLayer.ForestTrainingObservationsCount),
                    AttributesToUseCount);
            }
            EntropyForest = new Forest(InformationGainType.Entropy, TreesByEntropy);
        }

        public static void GenerateGiniIndexForest()
        {
            TreesByGiniIndex = new Tree[TreeLayer.ForestTreesCount];
            for (int i = 0; i < ForestTreesCount; i++)
            {
                TreesByGiniIndex[i] = treeConstructor.ConstructTree(InformationGainType.GiniIndex,
                    DataLayer.GetRandomSetOfInstances(TreeLayer.ForestTrainingObservationsCount),
                    DataLayer.GetRandomSetOfInstances(TreeLayer.ForestTrainingObservationsCount),
                    AttributesToUseCount);
            }
            GiniIndexForest = new Forest(InformationGainType.GiniIndex, TreesByGiniIndex);
        }

        public static void GenerateEntorpyFullTree()
        {
            if (TreeLayer.ShouldPrune)
            {
                EntropyFullTree = treeConstructor.ConstructTree(InformationGainType.Entropy,
                    DataLayer.Instances.Take<Instance>((int)(DataLayer.Instances.Length * 0.6f)).ToList<Instance>(),
                    DataLayer.Instances.Skip<Instance>((int)(DataLayer.Instances.Length * 0.2f)).Take<Instance>((int)(DataLayer.Instances.Length * 0.2f)).ToList<Instance>(),
                    DataLayer.Instances[0].attributes.Length);
            }
            else
            {
                EntropyFullTree = treeConstructor.ConstructTree(InformationGainType.Entropy,
                    DataLayer.Instances.Take<Instance>((int)(DataLayer.Instances.Length * 0.7f)).ToList<Instance>(),
                    null,
                    DataLayer.Instances[0].attributes.Length);
            }
        }

        public static void GenerateGiniIndexFullTree()
        {
            if (TreeLayer.ShouldPrune)
            {
                GiniIndexFullTree = treeConstructor.ConstructTree(InformationGainType.GiniIndex,
                    DataLayer.Instances.Take<Instance>((int)(DataLayer.Instances.Length * 0.6f)).ToList<Instance>(),
                    DataLayer.Instances.Skip<Instance>((int)(DataLayer.Instances.Length * 0.2f)).Take<Instance>((int)(DataLayer.Instances.Length * 0.2f)).ToList<Instance>(),
                    DataLayer.Instances[0].attributes.Length);
            }
            else
            {
                GiniIndexFullTree = treeConstructor.ConstructTree(InformationGainType.GiniIndex,
                    DataLayer.Instances.Take<Instance>((int)(DataLayer.Instances.Length * 0.7f)).ToList<Instance>(),
                    null,
                    DataLayer.Instances[0].attributes.Length);
            }
        }
    }
}