﻿namespace MSI2Project
{
    using System.Collections.Generic;

    public class TreeNode
    {
        public int SplitAttributeIndex { get; set; }

        public double SplitAttributeGainInformationValue { get; set; }

        public double SplitValue { get; set; }

        public string Category { get; set; }

        public Dictionary<string, TreeNode> Children {get; set;}

        /// <summary>
        /// Leaf constructor
        /// </summary>
        /// <param name="category"></param>
        public TreeNode(string category)
        {
            this.Category = category;
        }

        public TreeNode(int splitAttributeIndex)
        {
            this.SplitAttributeIndex = splitAttributeIndex;
        }
    }
}
