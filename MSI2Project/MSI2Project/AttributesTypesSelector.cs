﻿namespace MSI2Project
{
    using System.Drawing;
    using System.Windows.Forms;

    public partial class AttributesTypesSelector : Form
    {
        private readonly int LabelPositionY = 10;

        private readonly int RadioBtnPositionY = 30;

        private readonly int LabelPositionX = 10;

        private readonly int StepX = 40;

        private RadioButton[] doubles;
        private RadioButton[] strings;

        public AttributesTypesSelector(int attributesCount)
        {
            InitializeComponent();
            this.ControlBox = false;

            doubles = new RadioButton[attributesCount];
            strings = new RadioButton[attributesCount];
            

            for(int i = 0 ; i < attributesCount ; i++)
            {
                var lbl = new Label();
                lbl.Parent = this.OptionsPanel;
                lbl.Text = (i+1).ToString();
                lbl.Location = new Point(LabelPositionX + i * StepX, LabelPositionY);
                lbl.Width = 20;

                var grp = new Panel();
                
                grp.Parent = this.OptionsPanel;
                grp.Location = new Point(LabelPositionX + i * StepX, RadioBtnPositionY);
                grp.Width = 20;
                grp.Height = 50;

                doubles[i] = new RadioButton();
                doubles[i].Parent = grp;
                doubles[i].Location = new Point(0, 0);

                strings[i] = new RadioButton();
                strings[i].Parent = grp;
                strings[i].Location = new Point(0,30);
                strings[i].Checked = true; 
            }
        }

        private void ConfirmBtn_Click(object sender, System.EventArgs e)
        {
            DataLayer.AttributesTypes = new AttributeType[this.doubles.Length];
            for (int i = 0; i < DataLayer.AttributesTypes.Length; i++)
            {
                if (this.doubles[i].Checked)
                {
                    DataLayer.AttributesTypes[i] = AttributeType.Numeric;
                }
                else
                {
                    DataLayer.AttributesTypes[i] = AttributeType.Text;
                }
            }

            this.Close();
        }
    }
}
