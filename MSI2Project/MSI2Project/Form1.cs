﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MSI2Project
{
    public partial class Form1 : Form
    {
        private bool isDataLoaded = false;
        private bool areForestsGenerated = false;
        private bool areInstancesCategorized = false;
        private TreeVisualizationHelper treeVisualizationHelper = null;

        public Form1()
        {
            InitializeComponent();
        }

        private void ChoseDataButton_Click(object sender, EventArgs e)
        {
            StreamReader fileStream = null;
            OpenFileDialog openFileDialog = new OpenFileDialog();

            openFileDialog.InitialDirectory = Directory.GetCurrentDirectory();
            openFileDialog.Filter = "txt files (*.txt)|*.txt|data files (*.data)|*.data|All files (*.*)|*.*";
            openFileDialog.RestoreDirectory = false;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                fileStream = new StreamReader(openFileDialog.FileName);
                DataLayer.ParseData(fileStream);
            }
            else
            {
                return;
            }

            ForestTrainingObservationsCountNumericUpDown.Minimum = 1;
            ForestTrainingObservationsCountNumericUpDown.Maximum = DataLayer.Instances.Length;

            ForestTreesCountNumericUpDown.Minimum = 1;
            ForestTreesCountNumericUpDown.Maximum = 1000;

            AttributesProcessedCountNumericUpDown.Minimum = 1;
            AttributesProcessedCountNumericUpDown.Maximum = DataLayer.Instances.First().attributes.Length;
            AttributesProcessedCountNumericUpDown.Value = DataLayer.Instances.First().attributes.Length;

            CategorizedObservationsCountNumericUpDown.Minimum = 1;
            CategorizedObservationsCountNumericUpDown.Maximum = DataLayer.Instances.Length;

            MessageBox.Show("Zbiór treningowy został wybrany prawidłowo.");

            isDataLoaded = true;
            areForestsGenerated = false;
            areInstancesCategorized = false;
        }


        private void GenerateForestsButton_Click(object sender, EventArgs e)
        {
            if (!isDataLoaded)
            {
                MessageBox.Show("Załaduj zbiór treningowy.");
                return;
            }

            TreeLayer.ForestTrainingObservationsCount = (int) ForestTrainingObservationsCountNumericUpDown.Value;
            TreeLayer.ForestTreesCount = (int) ForestTreesCountNumericUpDown.Value;
            TreeLayer.AttributesToUseCount = (int)AttributesProcessedCountNumericUpDown.Value;

            TreeLayer.GenerateEntropyForest();
            TreeLayer.GenerateEntorpyFullTree();
            TreeLayer.GenerateGiniIndexForest();
            TreeLayer.GenerateGiniIndexFullTree();

            MessageBox.Show("Generowanie lasów zakończone.");

            areForestsGenerated = true;
            areInstancesCategorized = false;
        }

        private void CategorizeButton_Click(object sender, EventArgs e)
        {
            forestLabel = string.Empty;
            if (!isDataLoaded || !areForestsGenerated)
            {
                MessageBox.Show("Załaduj zbiór treningowy i wygeneruj lasy.");
                return;
            }

            StatisticsLabel.Text = "";

            var categorizedObservations =
                DataLayer.GetRandomSetOfInstances((int) CategorizedObservationsCountNumericUpDown.Value);

            var entropyStatistics = new Statistics(TreeLayer.EntropyForest, categorizedObservations);
            var giniIndexStatistics = new Statistics(TreeLayer.GiniIndexForest, categorizedObservations);

            entropyStatistics.SetStatisticsLabelForForests(InformationGainType.Entropy, StatisticsLabel);
            giniIndexStatistics.SetStatisticsLabelForForests(InformationGainType.GiniIndex, StatisticsLabel);

            //Tree Visualization
            treeVisualizationHelper = new TreeVisualizationHelper(this);
            treeVisualizationHelper.SetTreeImage();

            areInstancesCategorized = true;
        }

        #region Tree visualization control

        private void PreviousTreeButton_Click(object sender, EventArgs e)
        {
            if (areInstancesCategorized == false)
            {
                MessageBox.Show("Sklasyfikuj dane.");
                return;
            }
            if (treeVisualizationHelper == null)
                return;
            if(!String.IsNullOrEmpty(forestLabel))
                StatisticsLabel.Text = forestLabel;
            treeVisualizationHelper.SetPreviousTreeImage();
        }

        private void NextTreeButton_Click(object sender, EventArgs e)
        {
            if (areInstancesCategorized == false)
            {
                MessageBox.Show("Sklasyfikuj dane.");
                return;
            }
            if (treeVisualizationHelper == null)
                return;
            if (!String.IsNullOrEmpty(forestLabel))
                StatisticsLabel.Text = forestLabel;
            treeVisualizationHelper.SetNextTreeImage();
        }


        private void EntropyGraphRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (treeVisualizationHelper == null)
                return;
            if (!String.IsNullOrEmpty(forestLabel))
                StatisticsLabel.Text = forestLabel;
            treeVisualizationHelper.SetTreeImage();
        }

        private void GiniGraphRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (treeVisualizationHelper == null)
                return;
            if (!String.IsNullOrEmpty(forestLabel))
                StatisticsLabel.Text = forestLabel;
            treeVisualizationHelper.SetTreeImage();
        }

        #endregion

        private void PrunningEnalbedradioButton_CheckedChanged(object sender, EventArgs e)
        {
            TreeLayer.ShouldPrune = !TreeLayer.ShouldPrune;
        }

        private string forestLabel = string.Empty;
        private void DisplayTreeButton_Click(object sender, EventArgs e)
        {
            if (areInstancesCategorized == false)
            {
                MessageBox.Show("Sklasyfikuj dane.");
                return;
            }
            if (treeVisualizationHelper == null)
                return;

            var categorizedObservations = TreeLayer.ShouldPrune ?
                DataLayer.Instances.Skip((int)(DataLayer.Instances.Count() * 0.8f)).Take<Instance>((int)(DataLayer.Instances.Count() * 0.2f)).ToList<Instance>()
                : DataLayer.Instances.Skip((int)(DataLayer.Instances.Count() * 0.7f)).ToList<Instance>();

            var entropyStatistics = new Statistics(TreeLayer.EntropyFullTree, categorizedObservations);
            var giniIndexStatistics = new Statistics(TreeLayer.GiniIndexFullTree, categorizedObservations);

            if(StatisticsLabel.Text.Contains("las"))
                forestLabel = StatisticsLabel.Text;
            StatisticsLabel.Text = string.Empty;
            entropyStatistics.SetStatisticsLabelForTree(InformationGainType.Entropy, StatisticsLabel);
            giniIndexStatistics.SetStatisticsLabelForTree(InformationGainType.GiniIndex, StatisticsLabel);

            treeVisualizationHelper.SetFullTreeImage();
        }
    }
}