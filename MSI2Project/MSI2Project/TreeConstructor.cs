﻿namespace MSI2Project
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;

    public class TreeConstructor
    {
        private const double MaxAttributeVariety = 0.8f;

        private TreePruner pruner;

        public TreeConstructor()
        {
            this.pruner = new TreePruner();
        }

        public Tree ConstructTree(InformationGainType informationGainType, IList<Instance> trainingSet, IList<Instance> pruningSet, int attributesToUseAmount)
        {
            if(DataLayer.Instances == null || DataLayer.Instances.Count() == 0)
            {
                throw new Exception("There is no observations set loaded!");
            }

            var calculator = new InformationGainCalculator();

            if (informationGainType == InformationGainType.Entropy)
            {
                var tree = new Tree(ConstructNode(trainingSet, DataLayer.GetRandomAttributesIndexesAsList(attributesToUseAmount), calculator.Entropy), trainingSet);
                if (TreeLayer.ShouldPrune)
                {
                    this.pruner.Prune(tree, pruningSet);
                }

                return tree;
            }
            else
            {
                var tree = new Tree(ConstructNode(trainingSet, DataLayer.GetRandomAttributesIndexesAsList(attributesToUseAmount), calculator.Neg_GiniIndex), trainingSet);

                if (TreeLayer.ShouldPrune)
                {
                    this.pruner.Prune(tree, pruningSet);
                }
                
                return tree;
            }
        }

        public TreeNode ConstructNode(
            IList<Instance> instances, 
            IList<int> attributes, 
            Func<int, IList<Instance>, IList<int>, InformationGain> splitAttributeFunction)
        {       
            var categoryOfFirstElement = instances[0].category;

            if(instances.All(instance => instance.category == categoryOfFirstElement))
            {
                return new TreeNode(categoryOfFirstElement);
            }

            int attributeIndex = -1; 
            double bestValue = double.MinValue;
            var bestInformationGain = new InformationGain();

            var uselessAttributes = new List<int>();

            foreach (var attribute in attributes)
            {
                if(DataLayer.AttributesTypes[attribute] == AttributeType.Text && DataLayer.AllowedAttributesValues[attribute].Count/(double)instances.Count > MaxAttributeVariety)
                {
                    uselessAttributes.Add(attribute);
                    continue;
                }
            }

            foreach (var uselessAttr in uselessAttributes)
            {
                attributes.Remove(uselessAttr);
            }

            foreach (var attribute in attributes)
            {
                var currentValue = splitAttributeFunction(attribute, instances, attributes);
                if (currentValue.gainValue > bestValue)
                {
                    bestValue = currentValue.gainValue;
                    attributeIndex = attribute;
                    bestInformationGain = currentValue;
                }
            }

            

            if (attributes.Count == 0)
            {
                return new TreeNode(DataLayer.GetMostPopularCategoryInSet(instances));
            }

            var retNode = new TreeNode(attributeIndex);
            retNode.Children = new Dictionary<string, TreeNode>();

            var mostPopularCategory = DataLayer.GetMostPopularCategoryInSet(instances);

            switch (bestInformationGain.attributeType)
            {
                case AttributeType.Text:
                    {
                        foreach (var attributeValue in DataLayer.AllowedAttributesValues[attributeIndex])
                        {
                            var childSet = instances.Where(instance => instance.attributes[attributeIndex] == attributeValue).ToList();

                            if (childSet.Count == 0)
                            {
                                retNode.Children.Add(attributeValue, new TreeNode(mostPopularCategory));
                                continue;
                            }

                            retNode.Children.Add(
                                attributeValue,
                                ConstructNode(
                                    childSet,
                                    attributes.Where(index => index != attributeIndex).ToList(),
                                    splitAttributeFunction));
                        }
                        break;
                    }
                case AttributeType.Numeric:
                    {
                        retNode.SplitValue = bestInformationGain.splitAttributeValue;

                        // < group
                        var childSet = instances.Where(instance => double.Parse(instance.attributes[attributeIndex], CultureInfo.InvariantCulture) < bestInformationGain.splitAttributeValue).ToList();

                        if (childSet.Count == 0)
                        {
                            retNode.Children.Add(string.Format("<{0}", bestInformationGain.splitAttributeValue), new TreeNode(mostPopularCategory));
                        }
                        else
                        {
                            retNode.Children.Add(
                                string.Format("<{0}", bestInformationGain.splitAttributeValue),
                                ConstructNode(
                                    childSet,
                                    attributes.Where(index => index != attributeIndex).ToList(),
                                    splitAttributeFunction));
                        }

                        // >= group
                        childSet = instances.Where(instance => double.Parse(instance.attributes[attributeIndex], CultureInfo.InvariantCulture) >= bestInformationGain.splitAttributeValue).ToList();

                        if (childSet.Count == 0)
                        {
                            retNode.Children.Add(string.Format(">={0}", bestInformationGain.splitAttributeValue), new TreeNode(mostPopularCategory));
                            break;
                        }

                        retNode.Children.Add(
                            string.Format(">={0}", bestInformationGain.splitAttributeValue),
                            ConstructNode(
                                childSet,
                                attributes.Where(index => index != attributeIndex).ToList(),
                                splitAttributeFunction));
                        break;
                    }
            }
            retNode.SplitAttributeGainInformationValue = bestInformationGain.gainValue;
            return retNode;
        } 
    } 
}
