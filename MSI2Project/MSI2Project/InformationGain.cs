﻿namespace MSI2Project
{
    public class InformationGain
    {
        public double gainValue { get; set; }

        /// <summary>
        /// Only for numeric attributes
        /// We will split instances into 2 groups 
        /// 1:  >= splitAttributeValue
        /// 2:  <  splitAttributeValue
        /// </summary>
        public double splitAttributeValue { get; set; }

        public int attributeIndex { get; set; }

        public AttributeType attributeType { get; set; }
    }
}
